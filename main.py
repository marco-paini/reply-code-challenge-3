import multiprocessing
import Polygon
import Polygon.Utils
import pyvisgraph
import random

for n in range(1, 5):
    input = open("input_%s.txt" % n)
    l = 0
    polygons = []
    for line in input:
        line = line.split(' ')
        if l == 0:
            area = Polygon.Polygon(
                ((line[0], line[1]), (line[0], line[3]), (line[2], line[3]), (line[2], line[1])))
            area.scale(1.2, 1.2)
            end = pyvisgraph.Point(line[2], line[3])
            start = pyvisgraph.Point(line[0], line[1])
        elif l != 1:
            polygon = area & Polygon.Polygon(
                ((line[0], line[1]), (line[2], line[3]), (line[4], line[5])))
            if bool(polygon):
                for p in polygons:
                    if p.overlaps(polygon):
                        polygons.remove(p)
                        polygon += p
                polygons.append(polygon)
        l += 1
    print area
    print polygons
    pypolygons = []
    for polygon in polygons:
        points = []
        for point in Polygon.Utils.pointList(Polygon.Utils.fillHoles(polygon)):
            points.append(pyvisgraph.Point(point[0] + random.uniform(-0.01, 0.01), point[1] + random.uniform(-0.01, 0.01)))
        pypolygons.append(points)
    graph = pyvisgraph.VisGraph()
    graph.build(pypolygons, multiprocessing.cpu_count(), True)
    output = open("output_%s.txt" % n, "w")
    path = graph.shortest_path(start, end)
    output.write("%s\n" % len(path))
    for point in path:
      output.write("%.0f %.0f\n" % (point.x, point.y))
    output.close()
